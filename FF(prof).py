import random
import copy
import tkinter

dim = 400
fen = tkinter.Tk()
liste = []
fen.title("Forest Fires")
fen.geometry('{}x{}'.format(dim,dim))
fen.resizable(width=False, height=False)
fond = tkinter.Canvas(fen, width=600, height=600, background="pink")
fond.pack()


class Tree:
    def __init__(self, state=0):
        self.state = state

    def display(self):
        if self.state == 0:
            print('.', end=" ")
        elif self.state == 1:
            print('A', end=" ")
        elif self.state == 2:
            print('F', end=" ")
        else:
            print('C', end=" ")

    def propagation(self, burning):
        if self.state == 1:
            if burning:
                self.state = 2
        elif self.state == 2:
            self.state = 3


class Forest:
    def __init__(self, rows, columns, proba):
        self.lignes = rows
        self.colonnes = columns
        self.grid = [[Tree() for i in range(0, columns)] for i in range(0, rows)]
        self.grid2 = []

        for i in self.grid:
            for tree in i:
                if random.randint(0, 100) <= proba:
                    tree.state = 1

        self.grid[random.randint(0, self.lignes - 1)][random.randint(0, self.colonnes - 1)].state = 2

    def display(self):
        for i in self.grid:
            for j in i:
                j.display()
            print("")

    def demande_next_gen(self):
        a = int(input("Tu veux passer combien de générations ? "))
        self.grid2 = copy.deepcopy(self.grid)
        if a == 1:
            return self.next_gen()
        else:
            for i in range(0, a - 1):
                self.next_gen()
            return self.next_gen()

    def next_gen(self):
        for x in range(0, len(self.grid)):
            for y in range(0, len(self.grid[x])):
                burning = False
                if self.grid[x][y].state == 1:
                    if y != 0:
                        if self.grid[x][y - 1].state == 2:
                            burning = True
                    if y != len(self.grid[x]) - 1:
                        if self.grid[x][y + 1].state == 2:
                            burning = True
                    if x != 0:
                        if self.grid[x - 1][y].state == 2:
                            burning = True
                    if x != len(self.grid) - 1:
                        if self.grid[x + 1][y].state == 2:
                            burning = True
                self.grid2[x][y].propagation(burning)

        self.grid = copy.deepcopy(self.grid2)

def Forest_fires():
    a = int(input("Quelle proba en pourcentage de faire des arbres ? "))
    b = int(input("Quelle nombre de lignes pour le terrain ? "))
    c = int(input("Quelle nombre de colonnes pour le terrain ? "))

    F = Forest(b, c, a)
    F.display()
    while True:
        F.demande_next_gen()
        F.display()

fen.mainloop()
Forest_fires()