import random
import copy


class Tree:
    def __init__(self):
        self.etat = 0

    def display(self):
        if self.etat == 0:
            return "."
        elif self.etat == 1:
            return "T"
        elif self.etat == 2:
            return "F"
        elif self.etat == 3:
            return "B"


class Forest:
    def __init__(self, rows, columns, proba):
        self.lignes = rows
        self.colonnes = columns
        self.proba = proba
        self.grid = [[Tree() for i in range(0, columns)]for i in range(0, rows)]
        self.grid2 = [[]*rows]

    def display(self, tableau):
        for i in range(0, len(tableau)):
            for j in tableau[i]:
                print(j.display(), " ", end="")
            print("")

    def trees(self):
        for i in self.grid:
            for arbre in i:
                if random.randint(0, 100) <= self.proba:
                    arbre.etat = 1

    def set_fire(self):
        self.grid[random.randint(0, self.lignes - 1)][random.randint(0, self.colonnes - 1)].etat = 2

    def demande_next_gen(self):
        a = int(input("Tu veux passer combien de générations ? "))
        self.grid2 = copy.deepcopy(self.grid)
        if a == 1:
            return self.next_gen()
        else:
            for i in range(0, a - 1):
                self.next_gen()
            return self.next_gen()

    def next_gen(self):

        for i in range(0, len(self.grid)):
            for arbre in range(0, len(self.grid[i])):
                if self.grid[i][arbre].etat == 2:
                    self.grid2[i][arbre].etat = 3
                if self.grid[i][arbre].etat == 1:
                    if arbre != 0:
                        if self.grid[i][arbre - 1].etat == 2:
                            self.grid2[i][arbre].etat = 2
                    if arbre != len(self.grid[i]) - 1:
                        if self.grid[i][arbre + 1].etat == 2:
                            self.grid2[i][arbre].etat = 2
                    if i != 0:
                        if self.grid[i - 1][arbre].etat == 2:
                            self.grid2[i][arbre].etat = 2
                    if i != len(self.grid) - 1:
                        if self.grid[i + 1][arbre].etat == 2:
                            self.grid2[i][arbre].etat = 2

        self.grid = copy.deepcopy(self.grid2)
        return self.grid


def Forest_fires():
    a = int(input("Quelle proba en pourcentage de faire des arbres ? "))
    b = int(input("Quelle nombre de lignes pour le terrain ? "))
    c = int(input("Quelle nombre de colonnes pour le terrain ? "))

    F = Forest(b, c, a)
    F.trees()
    F.set_fire()
    F.display(F.grid)
    while True:
        F.display(F.demande_next_gen())


Forest_fires()
