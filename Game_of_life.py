import random
import copy
import time


class Cellule:
    def __init__(self):
        self.etat = 0
        self.voisins = 0

    def display(self):
        if self.etat == 0:
            return " "
        else:
            return "X"


class Terrain:
    def __init__(self, rows, columns, proba):
        self.lignes = rows
        self.colonnes = columns
        self.grid = [[Cellule() for i in range(0, columns)]for i in range(0, rows)]
        self.grid2 = [[]*rows]
        self.proba = proba

    def display(self):
        for i in self.grid:
            for j in i:
                print(j.display(), " ", end="")
            print("")

    def vie(self):
        for i in self.grid:
            for cellule in i:
                if random.randint(0, 100) <= self.proba:
                    cellule.etat = 1

    def next_gen(self):
        time.sleep(0.2)
        self.grid2 = copy.deepcopy(self.grid)

        for i in range(0, len(self.grid)):
            for cellule in range(0, len(self.grid[i])):
                self.grid[i][cellule].voisins = 0
                if cellule != 0:
                    if self.grid[i][cellule - 1].etat == 1:
                        self.grid[i][cellule].voisins += 1
                    if i != 0:
                        if self.grid[i - 1][cellule - 1].etat == 1:
                            self.grid[i][cellule].voisins += 1
                    if i != len(self.grid) - 1:
                        if self.grid[i + 1][cellule - 1].etat == 1:
                            self.grid[i][cellule].voisins += 1

            #rajouter les conditions pour le monde thorique

                if cellule != len(self.grid[i]) - 1:
                    if self.grid[i][cellule + 1].etat == 1:
                        self.grid[i][cellule].voisins += 1
                    if i != 0:
                        if self.grid[i - 1][cellule + 1].etat == 1:
                            self.grid[i][cellule].voisins += 1
                    if i != len(self.grid) - 1:
                        if self.grid[i + 1][cellule + 1].etat == 1:
                            self.grid[i][cellule].voisins += 1
                if i != 0:
                    if self.grid[i - 1][cellule].etat == 1:
                        self.grid[i][cellule].voisins += 1
                if i != len(self.grid) - 1:
                    if self.grid[i + 1][cellule].etat == 1:
                        self.grid[i][cellule].voisins += 1

                if self.grid[i][cellule].voisins == 3:
                    self.grid2[i][cellule].etat = 1
                elif self.grid[i][cellule].voisins <= 1 or self.grid[i][cellule].voisins >= 4:
                    self.grid2[i][cellule].etat = 0

        self.grid = copy.deepcopy(self.grid2)
        return self.grid


def Game_of_Life():
    a = int(input("Quelle proba en pourcentage de faire des cellules ? "))
    b = int(input("Quelle nombre de lignes pour le terrain ? "))
    c = int(input("Quelle nombre de colonnes pour le terrain ? "))

    T = Terrain(b, c, a)
    T.vie()
    T.display()
    while True:
        T.next_gen()
        print("-"*T.colonnes * 3)
        T.display()
        print("-"*T.colonnes * 3)

Game_of_Life()
