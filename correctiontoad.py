def initBoard(n, p):
    if p >= n/2:
        p = int(n/2 - 1)
    board = [0] * n
    for i in range(p):
        board[i] = 1
        board[n - 1 - i] = 2
    return board
def display(board, n):
    for item in board:
        if item == 0:
            print(".", end="\t")
        elif item == 1:
            print("X", end="\t")
        else:
            print("O", end="\t")
    print()
    for i in range(n):
        print(i, end="\t")
    print()
def possible(board, n, i, player):
    if player == 1:
        # Vérification pion et bord du plateau
        if board[i] != player or i + 1 >= n:
            return False
        if board[i + 1] == 0:
            return True
        if i + 2 >= n:
            return False
        if board[i + 1] == 2 and board[i + 2] == 0:
            return True
    else:
        # Vérification pion et bord du plateau
        if board[i] != player or i - 1 < 0:
            return False
        if board[i - 1] == 0:
            return True
        if i - 2 < 0:
            return False
        if board[i - 1] == 1 and board[i - 2] == 0:
            return True
    return False
def select(board, n, player):
    ok = False
    while not ok:
        i = int(input('Choix du pion ?'))
        ok = possible(board, n, i, player)
    return i
def move(board, n, player, i):
    board[i] = 0
    if player == 1:
        if board[i + 1] == 0:
            board[i + 1] = 1
        else:
            board[i + 2] = 1
    else:
        if board[i - 1] == 0:
            board[i - 1] = 2
        else:
            board[i - 2] = 2
def again(board, n, player):
    for i in range(n):
        if possible(board, n, i, player):
            return True
    return False
def toadsAndFrogs(n, p):
    player = 1
    board = initBoard(n, p)
    while again(board, n, player):
        print("Au tour du joueur ", player)
        display(board, n)
        i = select(board, n, player)
        move(board, n, player, i)
        display(board, n)
        if player == 1:
            player = 2
        else:
            player = 1
    if player == 1:
        player = 2
    else:
        player = 1
    print("Le joueur ", player, " a gagné !")
n = int(input("Taille du plateau : "))
p = int(input("Nombre de pion : "))
toadsAndFrogs(n, p)